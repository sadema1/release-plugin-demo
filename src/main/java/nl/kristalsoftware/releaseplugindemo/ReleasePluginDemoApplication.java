package nl.kristalsoftware.releaseplugindemo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ReleasePluginDemoApplication {

    @Value("${application.message}")
    private String message;

    public static void main(String[] args) {
        SpringApplication.run(ReleasePluginDemoApplication.class, args);
    }

    @Bean
    CommandLineRunner showMessage() {
        return args -> {
            System.out.println(message);
        };
    }
}
