FROM eclipse-temurin:17-jdk-alpine

# Add app jar
COPY target/release-plugin-demo-*.jar /opt/release-demo/release-plugin-demo.jar

ENV ENV=dev
EXPOSE 8080

WORKDIR /opt/release-demo
ENTRYPOINT [ "java", "-Duser.timezone=Europe/Amsterdam", "-jar", "release-plugin-demo.jar" ]
