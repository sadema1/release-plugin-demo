
```shell
kubectl create deployment release-plugin-demo --image release-demo --port=8080 --replicas=1 --dry-run=client -o yaml > release-plugin-deployment.yml
```

```shell
kubectl create cm release-demo --dry-run=client -o yaml  > release-plugin-configmap.yml
```

```shell
mvn -B -DreleaseVersion="2.2.0" -DdevelopmentVersion="2.3.0-SNAPSHOT" gitflow:release
```

```shell
// Branch: develop, pom version: 2.4.0-SNAPSHOT
// Branch: main,    pom version: 2.3.0
// tag: 2.3.0

mvn -B -DdevelopmentVersion="2.5.0-SNAPSHOT" gitflow:release

// Result:
// Branch: develop, pom version: 2.5.0-SNAPSHOT
// Branch: main,    pom version: 2.4.0
// tag: 2.4.0
```

```shell
// Branch: develop, pom version: 2.5.0-SNAPSHOT
// Branch: main,    pom version: 2.4.0
// tag: 2.4.0

mvn -B gitflow:release-start
// Result: active branch: release/2.5.0

// Upload the release jar to Artifactory

mvn -B gitflow:release-finish
// Result: active branch: develop
// tag: 2.5.0
```

-versionDigitToIncrement

```shell
mvn -B gitflow:release-start
[INFO] Scanning for projects...
[INFO] 
[INFO] ---------------< nl.kristalsoftware:release-plugin-demo >---------------
[INFO] Building release-plugin-demo 2.5.0-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
[INFO] 
[INFO] --- gitflow-maven-plugin:1.20.0:release-start (default-cli) @ release-plugin-demo ---
[INFO] Checking for uncommitted changes.
[INFO] Fetching remote from 'origin'.
[INFO] Comparing local branch 'develop' with remote 'origin/develop'.
[INFO] Checking out 'develop' branch.
[INFO] Checking for SNAPSHOT versions in dependencies.
[INFO] Version is blank. Using default version.
[INFO] Creating a new branch 'release/2.5.0' from 'develop' and checking it out.
[INFO] Updating version(s) to '2.5.0'.
[INFO] Committing changes.
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  6.553 s
[INFO] Finished at: 2024-08-20T15:47:27+02:00
[INFO] ------------------------------------------------------------------------
```

```shell
mvn -B gitflow:release-finish
[INFO] Scanning for projects...
[INFO] 
[INFO] ---------------< nl.kristalsoftware:release-plugin-demo >---------------
[INFO] Building release-plugin-demo 2.5.0
[INFO] --------------------------------[ jar ]---------------------------------
[INFO] 
[INFO] --- gitflow-maven-plugin:1.20.0:release-finish (default-cli) @ release-plugin-demo ---
[INFO] Checking for uncommitted changes.
[INFO] Checking out 'release/2.5.0' branch.
[INFO] Checking for SNAPSHOT versions in dependencies.
[INFO] Fetching remote from 'origin'.
[INFO] Fetching remote from 'origin'.
[INFO] Comparing local branch 'develop' with remote 'origin/develop'.
[INFO] Fetching remote from 'origin'.
[INFO] Comparing local branch 'main' with remote 'origin/main'.
[INFO] Checking out 'release/2.5.0' branch.
[INFO] Checking out 'main' branch.
[INFO] Merging (--no-ff) 'release/2.5.0' branch.
[INFO] Creating '2.5.0' tag.
[INFO] Checking out 'develop' branch.
[INFO] Merging (--no-ff) '2.5.0' branch.
[INFO] Updating version(s) to '2.5.1-SNAPSHOT'.
[INFO] Committing changes.
[INFO] Pushing 'main' branch to 'origin'.
[INFO] Pushing 'develop' branch to 'origin'.
[INFO] ------------------------------------------------------------------------
```

```shell
mvn -B -DversionDigitToIncrement=1 gitflow:release-finish
```
